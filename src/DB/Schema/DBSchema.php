<?php namespace DBDiff\DB\Schema;

use Diff\Differ\ListDiffer;

use DBDiff\Params\ParamsFactory;
use DBDiff\Diff\SetDBCollation;
use DBDiff\Diff\SetDBCharset;
use DBDiff\Diff\DropTable;
use DBDiff\Diff\AddTable;
use DBDiff\Diff\AlterTable;
use DBDiff\Diff\AddView;
use DBDiff\Logger;


class DBSchema {

    function __construct($manager) {
        $this->manager = $manager;
    }
    
    function getDiff() {
        $params = ParamsFactory::get();

        $diffs = [];

        // Collation
        $dbName = $this->manager->getDB('target')->getDatabaseName();
        $sourceCollation = $this->getDBVariable('source', 'collation_database');
        $targetCollation = $this->getDBVariable('target', 'collation_database');
        if ($sourceCollation !== $targetCollation) {
            $diffs[] = new SetDBCollation($dbName, $sourceCollation, $targetCollation);
        }

        // Charset
        $sourceCharset = $this->getDBVariable('source', 'character_set_database');
        $targetCharset = $this->getDBVariable('target', 'character_set_database');
        if ($sourceCharset !== $targetCharset) {
            $diffs[] = new SetDBCharset($dbName, $sourceCharset, $targetCharset);
        }
        
        // Tables
        $tableSchema = new TableSchema($this->manager);

        $sourceTables = $this->manager->getTables('source');
        $targetTables = $this->manager->getTables('target');

        if (isset($params->tablesToIgnore)) {            
            $test = function($item) use ($params) {

                foreach ($params->tablesToIgnore as $table) {
                    if (strcasecmp($table,$item) === 0 ) {
                        return false;
                    }
                    if ($table[0] === '/' && \preg_match($table,$item) ) {
                        return false;
                    }
                }
                return true;
            };

            $sourceTables = array_filter($sourceTables, $test);
            $targetTables = array_filter($targetTables, $test);

        }
        $addedTables = array_diff($sourceTables, $targetTables);
        foreach ($addedTables as $table) {
            $sourceSchema = (object) $tableSchema->getSchema('source',$table);
            if ( $sourceSchema->view ) {
                 $diffs[] = new AddView($table, $this->manager->getDB('source'));
                 Logger::info("Adding source view `$table`");
                }
            else {
                Logger::info("Adding table `$table`");
                $diffs[] = new AddTable($table, $this->manager->getDB('source'));
            }
        }

        $commonTables = array_intersect($sourceTables, $targetTables);
        foreach ($commonTables as $table) {
            $tableDiff = $tableSchema->getDiff($table);
            $diffs = array_merge($diffs, $tableDiff);
        }

        $deletedTables = array_diff($targetTables, $sourceTables);
        foreach ($deletedTables as $table) {
            $targetSchema = (object) $tableSchema->getSchema('target',$table);
            if ( $targetSchema->view ) {
                $diffs[] = new AddView($table, $this->manager->getDB('target'));
                 Logger::info("Adding target view `$table`");
                }
            else {
                $diffs[] = new DropTable($table, $this->manager->getDB('target'));
                Logger::info("Dropping table `$table`");
            }
        }

        //foreach ($this->manager-getEvents($conne))
        return $diffs;
    }

    protected function getDBVariable($connection, $var) {
        $result = $this->manager->getDB($connection)->select("show variables like '$var'");
        return $result[0]['Value'];
    }

}
