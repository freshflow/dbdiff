<?php namespace DBDiff\DB\Schema;

use DBDiff\Params\ParamsFactory;
use Diff\Differ\MapDiffer;
use Diff\Differ\ListDiffer;

use DBDiff\Diff\AlterTableEngine;
use DBDiff\Diff\AlterTableCollation;

use DBDiff\Diff\AlterTableAddColumn;
use DBDiff\Diff\AlterTableChangeColumn;
use DBDiff\Diff\AlterTableDropColumn;

use DBDiff\Diff\AlterTableAddKey;
use DBDiff\Diff\AlterTableChangeKey;
use DBDiff\Diff\AlterTableDropKey;

use DBDiff\Diff\AlterTableAddConstraint;
use DBDiff\Diff\AlterTableChangeConstraint;
use DBDiff\Diff\AlterTableDropConstraint;
use DBDiff\Diff\AddView;

use DBDiff\SQLGen\Schema\SQL;

use DBDiff\Logger;


class TableSchema {

    function __construct($manager) {
        $this->manager = $manager;
        $this->source = $this->manager->getDB('source');
        $this->target = $this->manager->getDB('target');
    }

    public function getSchema($connection, $table) {
        
        if (!in_array($connection,['source','target']) ) {
            throw new \Exception;
        }
        // collation & engine
        $status = $this->{$connection}->select("show table status like '$table'");
        $engine = $status[0]['Engine'] ?:  $status[0]['Comment'] ;
        $collation = $status[0]['Collation'];
        
        $schema = $this->{$connection}->select("SHOW CREATE TABLE `$table`")[0]['Create Table'];
        $lines = array_map(function($el) { return trim($el);}, explode("\n", $schema));
        $lines = array_slice($lines, 1, -1);
        
        $columns = [];
        $keys = [];
        $constraints = [];
        
        foreach ($lines as $line) {
            preg_match("/`([^`]+)`/", $line, $matches);
            $name = $matches[1];
            $line = trim($line, ',');
            if (starts_with($line, '`')) { // column
                $columns[$name] = $line;
            } else if (starts_with($line, 'CONSTRAINT')) { // constraint
                $constraints[$name] = $line;
            } else { // keys
                $keys[$name] = $line;
            }
        }

        return [
            'engine'      => $engine,
            'collation'   => $collation,
            'columns'     => $columns,
            'keys'        => $keys,
            'constraints' => $constraints,
            'view'        => (\strcasecmp($engine,'VIEW')===0)
        ];
    }

    public function getDiff($table) {
        
        $params = ParamsFactory::get();


        $diffSequence = [];
        $sourceSchema = $this->getSchema('source', $table);
        $targetSchema = $this->getSchema('target', $table);

        //Views
        if ($sourceSchema['view'] || $targetSchema['view']) {
            Logger::info("Changing view `$table`");
            $diffSequence[] = new AddView($table, $this->manager->getDB('source'),$this->manager->getDB('target'));
            return $diffSequence;
        }
        Logger::info("Now calculating schema diff for table `$table`");
        // Engine
        $sourceEngine = $sourceSchema['engine'];
        $targetEngine = $targetSchema['engine'];

        if ($sourceEngine != $targetEngine) {
            $diffSequence[] = new AlterTableEngine($table, $sourceEngine, $targetEngine);
        }

        // Collation
        $sourceCollation = $sourceSchema['collation'];
        $targetCollation = $targetSchema['collation'];
        if ($sourceCollation != $targetCollation) {
            $diffSequence[] = new AlterTableCollation($table, $sourceCollation, $targetCollation);
        }

        // Columns
        $sourceColumns = $sourceSchema['columns'];
        $targetColumns = $targetSchema['columns'];

        $filterSource = function ($it) { return !empty($it); };
        $filterTarget = function ($it) { return !empty($it); };

        if (isset($params->fieldsToIgnore) && isset($params->fieldsToIgnore[$table])) {            
            $fieldsToIgnore = $params->fieldsToIgnore[$table];
            Logger::info("Fields ignored for table `$table`");
            $test = function($item) use ($fieldsToIgnore) {

                foreach ($fieldsToIgnore as $field) {
                    Logger::info($field.' '.$item);
                    if (strcasecmp($field,$item) === 0 ) {
                        return false;
                    }
                    if ($field[0] === '/' && \preg_match($field,$item,$m) ) {
                        return false;
                    }
                }
                return true;
            };

            $sourceColumns = array_filter($sourceColumns,$test,ARRAY_FILTER_USE_KEY);
            $targetColumns = array_filter($targetColumns,$test,ARRAY_FILTER_USE_KEY);

            $filterTarget = function($item) use ($fieldsToIgnore) {
                foreach ($targetColumns as $column) { 
                    Logger::info('key t '.$column.' '.$item);
                    if ($column[0] === '/') {
                        $column = str_replace(['/^','/'],['`',''],$column);
                    }
                    if ( stripos($item,$column) !== FALSE) {
                        return false; 
                    } 
                }
                return true; 
            };
            $filterSource = $filterTarget;
        }
    
        $differ = new MapDiffer();
        $diffs = $differ->doDiff($targetColumns, $sourceColumns);
        foreach ($diffs as $column => $diff) {
            if ($diff instanceof \Diff\DiffOp\DiffOpRemove) {
                $diffSequence[] = new AlterTableDropColumn($table, $column, $diff);
            } else if ($diff instanceof \Diff\DiffOp\DiffOpChange) {
                $diffSequence[] = new AlterTableChangeColumn($table, $column, $diff);
            } else if ($diff instanceof \Diff\DiffOp\DiffOpAdd) {
                $diffSequence[] = new AlterTableAddColumn($table, $column, $diff);
            }
        }

        // Keys
        if (!$params->skipKeys) {
            $sourceKeys = array_filter($sourceSchema['keys'],$filterSource); 
            $targetKeys = array_filter($targetSchema['keys'],$filterTarget);
            $differ = new MapDiffer();
            $diffs = $differ->doDiff($targetKeys, $sourceKeys);
            foreach ($diffs as $key => $diff) {
                if ($diff instanceof \Diff\DiffOp\DiffOpRemove) {
                    $diffSequence[] = new AlterTableDropKey($table, $key, $diff);
                } else if ($diff instanceof \Diff\DiffOp\DiffOpChange) {
                    $diffSequence[] = new AlterTableChangeKey($table, $key, $diff);
                } else if ($diff instanceof \Diff\DiffOp\DiffOpAdd) {
                    $diffSequence[] = new AlterTableAddKey($table, $key, $diff);
                }
            }
        }

        // Constraints
        if (!$params->skipConstraints) {
            $sourceConstraints = array_filter($sourceSchema['constraints'],$filterSource);
            $targetConstraints = array_filter($targetSchema['constraints'],$filterTarget);
            $differ = new MapDiffer();
            $diffs = $differ->doDiff($targetConstraints, $sourceConstraints);
            foreach ($diffs as $name => $diff) {
                if ($diff instanceof \Diff\DiffOp\DiffOpRemove) {
                    $diffSequence[] = new AlterTableDropConstraint($table, $name, $diff);
                } else if ($diff instanceof \Diff\DiffOp\DiffOpChange) {
                    $diffSequence[] = new AlterTableChangeConstraint($table, $name, $diff);
                } else if ($diff instanceof \Diff\DiffOp\DiffOpAdd) {
                    $diffSequence[] = new AlterTableAddConstraint($table, $name, $diff);
                }
            }
        }
        return $diffSequence;
    }

}
