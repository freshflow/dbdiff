<?php namespace DBDiff\SQLGen\DiffToSQL;

use DBDiff\SQLGen\SQLGenInterface;


class AddViewSQL implements SQLGenInterface {

    function __construct($obj) {
        $this->obj = $obj;
    }
    
    public function getUp() {
        $table = $this->obj->table;
        $connection = $this->obj->connectionUp;
        $res = $connection->select("SHOW CREATE VIEW `$table`");
        return  "DROP VIEW IF EXISTS `$table`;\n".
                preg_replace('/DEFINER=.* SQL SECURITY DEFINER/','',$res[0]['Create View']).';';
    }

    public function getDown() {
        $table = $this->obj->table;
        $connection = $this->obj->connectionDown;
        if ( empty($connection) ) {
            return "DROP VIEW IF EXISTS `$table`;";
        } 
        else {
            $res = $connection->select("SHOW CREATE VIEW `$table`");
            return  "DROP VIEW IF EXISTS `$table`;\n".
                    preg_replace('/DEFINER=.* SQL SECURITY DEFINER/','',$res[0]['Create View']).';';    
        }
    }
}
