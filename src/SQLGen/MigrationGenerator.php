<?php namespace DBDiff\SQLGen;


class MigrationGenerator {

    public static function generate($diffs, $method) {
        $sql = "";
        $lastTable = '';
        $lastCat = '';
        $sql .= "SET FOREIGN_KEY_CHECKS = 0; \n";
        foreach ($diffs as $diff) {

            $reflection = new \ReflectionClass($diff);
            $sqlGenClass = __NAMESPACE__."\\DiffToSQL\\".$reflection->getShortName()."SQL";
            $gen = new $sqlGenClass($diff);
            if ($lastCat !== $reflection->getShortName()) {
                $lastCat = $reflection->getShortName();
                $sql .= "\n\n-- ".$reflection->getShortName()." --\n";
            }

            if (isset($diff->table) && $lastTable !== $diff->table) {
                $lastTable = $diff->table;
                $sql .= "\n-- TABLE: ".$diff->table." --\n";
            }
            
            $sql .= $gen->$method()."\n";
        }
        $sql .= "SET FOREIGN_KEY_CHECKS = 1; \n";
        return $sql;
    }

}
