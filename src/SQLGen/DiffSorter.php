<?php namespace DBDiff\SQLGen;


class DiffSorter {

    private $up_order = [
        "SetDBCharset",
        "SetDBCollation",

        "AlterTableAddColumn",
        "AlterTableChangeColumn",
        "AlterTableDropColumn",

        "AddTable",

        "DeleteData",
        "DropTable",

        "AlterTableEngine",
        "AlterTableCollation",

        "AddView",

        "AlterTableAddKey",
        "AlterTableChangeKey",
        "AlterTableDropKey",

        "AlterTableAddConstraint",
        "AlterTableChangeConstraint",
        "AlterTableDropConstraint",

        "InsertData",
        "UpdateData"
    ];

    private $down_order = [
        "SetDBCharset",
        "SetDBCollation",

        "InsertData",
        "AddTable",

        "AlterTableAddColumn",
        "AlterTableChangeColumn",
        "AlterTableDropColumn",

        "DropTable",

        "AlterTableEngine",
        "AlterTableCollation",

        "AddView",

        "AlterTableAddKey",
        "AlterTableChangeKey",
        "AlterTableDropKey",

        "AlterTableAddConstraint",
        "AlterTableChangeConstraint",
        "AlterTableDropConstraint",

        "DeleteData",
        "UpdateData"
    ];

    public function sort($diff, $type) {
        usort($diff, [$this, 'compare'.ucfirst($type)]);
        return $diff;
    }
    
    private function compareUp($a, $b) {
        return $this->compare($this->up_order, $a, $b);
    }

    private function compareDown($a, $b) {
        return $this->compare($this->down_order, $a, $b);
    }

    private function compare($order, $a, $b) {
        $order = array_flip($order);
        $reflectionA = new \ReflectionClass($a);
        $reflectionB = new \ReflectionClass($b);
        $sqlGenClassA = $reflectionA->getShortName();
        $sqlGenClassB = $reflectionB->getShortName();
        $indexA = $order[$sqlGenClassA];
        $indexB = $order[$sqlGenClassB];
        
        $tA = isset($a->table) ? $a->table : null;
        $tB = isset($b->table) ? $b->table : null;
        if ($indexA === $indexB) {
            if ($tA === null || $tB === null) {
                return 0;
            }
            return \strcasecmp($tA,$tB);
        }
        else if ($indexA > $indexB) return 1;
        return -1;
    }
}
